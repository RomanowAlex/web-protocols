package ru.romanow.protocols.soap.generated.wrapped.model;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceFeature;
import javax.xml.ws.Service;

/**
 * This class was generated by Apache CXF 3.2.0
 * 2017-10-03T14:02:44.252+03:00
 * Generated source version: 3.2.0
 * 
 */
@WebServiceClient(name = "WebServiceDocumentLiteralWrappedImplService", 
                  wsdlLocation = "file:/home/romanow/Develop/WebProtocols/soap-client/src/main/resources/wsdl/document-literal-wrapped.wsdl",
                  targetNamespace = "http://web.soap.protocols.romanow.ru/") 
public class WebServiceDocumentLiteralWrappedImplService extends Service {

    public final static URL WSDL_LOCATION;

    public final static QName SERVICE = new QName("http://web.soap.protocols.romanow.ru/", "WebServiceDocumentLiteralWrappedImplService");
    public final static QName WebServiceDocumentLiteralWrappedImplPort = new QName("http://web.soap.protocols.romanow.ru/", "WebServiceDocumentLiteralWrappedImplPort");
    static {
        URL url = null;
        try {
            url = new URL("file:/home/romanow/Develop/WebProtocols/soap-client/src/main/resources/wsdl/document-literal-wrapped.wsdl");
        } catch (MalformedURLException e) {
            java.util.logging.Logger.getLogger(WebServiceDocumentLiteralWrappedImplService.class.getName())
                .log(java.util.logging.Level.INFO, 
                     "Can not initialize the default wsdl from {0}", "file:/home/romanow/Develop/WebProtocols/soap-client/src/main/resources/wsdl/document-literal-wrapped.wsdl");
        }
        WSDL_LOCATION = url;
    }

    public WebServiceDocumentLiteralWrappedImplService(URL wsdlLocation) {
        super(wsdlLocation, SERVICE);
    }

    public WebServiceDocumentLiteralWrappedImplService(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public WebServiceDocumentLiteralWrappedImplService() {
        super(WSDL_LOCATION, SERVICE);
    }
    
    public WebServiceDocumentLiteralWrappedImplService(WebServiceFeature ... features) {
        super(WSDL_LOCATION, SERVICE, features);
    }

    public WebServiceDocumentLiteralWrappedImplService(URL wsdlLocation, WebServiceFeature ... features) {
        super(wsdlLocation, SERVICE, features);
    }

    public WebServiceDocumentLiteralWrappedImplService(URL wsdlLocation, QName serviceName, WebServiceFeature ... features) {
        super(wsdlLocation, serviceName, features);
    }    




    /**
     *
     * @return
     *     returns WebServiceDocumentWrapped
     */
    @WebEndpoint(name = "WebServiceDocumentLiteralWrappedImplPort")
    public WebServiceDocumentWrapped getWebServiceDocumentLiteralWrappedImplPort() {
        return super.getPort(WebServiceDocumentLiteralWrappedImplPort, WebServiceDocumentWrapped.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns WebServiceDocumentWrapped
     */
    @WebEndpoint(name = "WebServiceDocumentLiteralWrappedImplPort")
    public WebServiceDocumentWrapped getWebServiceDocumentLiteralWrappedImplPort(WebServiceFeature... features) {
        return super.getPort(WebServiceDocumentLiteralWrappedImplPort, WebServiceDocumentWrapped.class, features);
    }

}
