package ru.romanow.protocols.rpc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by ronin on 18.09.16
 */
@SpringBootApplication
public class XmlRpcServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(XmlRpcServerApplication.class, args);
    }
}
